package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

//ESta anotacion indica la url que ebe utilizar esta prueba
@DefaultUrl("https://www.google.com.co/")
public class GooglePage extends PageObject{
	
	//Esta anotacio antes de la declariacion de una variable, ayuda a mapear los elementos y a guradarlos en una variable
	@FindBy(name="q")
	WebElement buscador;
	
	public void escribeEnLaBarraDeBusqueda() {
		buscador.sendKeys("Hola Mundo \n");
	}
}
