package features.busqueda;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import pasos.TesterSteps;

@RunWith(SerenityRunner.class) //Garantiza que serenity regitre e informe los resultados de la prueba
public class HolaMundo {

	//manage permite que serenity administre el navegador en cuanto a la apertura y cierre de este cuando finalice la prueba
	@Managed(driver = "chrome")
	public WebDriver driver;
	
	@Steps
	public TesterSteps tester;
	
	//test indica los casos de ruebas que se van a correr
	@Test
	public void busquedaDeHolaMundoEnGoogle() {
		tester.abreElNavegador();
		tester.escribeEnLaBarraDeBusqueda();
	}
	
}
