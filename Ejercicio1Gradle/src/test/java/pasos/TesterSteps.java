package pasos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pages.GooglePage;

public class TesterSteps {

	GooglePage googlepage;
	
	@Step
	public void abreElNavegador() {
		googlepage.open();//open() es un metodo de serenity
	}
	
	//El texto como parametro en la anotacion de Step, aparecera como lineas en el informe
	@Step
	public void escribeEnLaBarraDeBusqueda() {
		googlepage.escribeEnLaBarraDeBusqueda();
	}
	
}
